const cleanDeep = require('clean-deep');

exports.clean = function (input, options, spacing) {
	return cleanJson(input, options, spacing);
};

function cleanJson(json, options, spacing) {
	return JSON.stringify(cleanDeep(
		(typeof json === 'string') ? JSON.parse(json) : json,
		options
	), null, spacing);
}
