#!/usr/bin/env node

const usage = `Usage: $0 [args]

If no input file is given, the program reads from STDIN.

Use $0 --help for help
Visit https://www.npmjs.com/package/clean-deep-cli for more information
`;
const argv = require('yargs')
	.usage(usage)
	.option('input', {
		alias: 'i',
		type: 'string',
		description: 'Input file name or "-" for standard input (stdin)',
		default: '-'
	})
	.option('output', {
		alias: 'o',
		type: 'string',
		description: 'Output file name or "-" for standard output (stdout)',
		default: '-'
	})
	.option('spacing', {
		alias: 's',
		type: 'number',
		default: 0,
		description: 'Spacing for output. (0 for none)'
	})
	.option('quiet', {
		alias: 'q',
		type: 'boolean',
		description: 'Supress all errors'
	})
	.option('verbose', {
		alias: 'v',
		type: 'boolean',
		description: 'Print stack trace on error (only if quiet is not set)'
	})
	.option('cleanKeys', {
		type: 'string',
		description: 'Specific keys to remove',
		default: []
	})
	.option('cleanValues', {
		type: 'string',
		description: 'Specific values to remove',
		default: []
	})
	.option('emptyArrays', {
		type: 'boolean',
		description: 'Remove empty arrays',
		default: true
	})
	.option('emptyObjects', {
		type: 'boolean',
		description: 'Remove empty objects',
		default: true
	})
	.option('emptyStrings', {
		type: 'boolean',
		description: 'Remove empty strings',
		default: true
	})
	.option('nullValues', {
		type: 'boolean',
		description: 'Remove null values',
		default: true
	})
	.option('undefinedValues', {
		type: 'boolean',
		description: 'Remove undefined values',
		default: true
	})
	.argv

const fs = require('fs');
const cleanDeep = require('../index');

process.on('uncaughtException', error => {
	if (!argv.quiet) {
		console.error(`Error: ${error.message}`);
		if (argv.verbose) console.error(error);
		process.exit(1)
	}
})

var options = {
	cleanKeys:  argv.cleanKeys,
	cleanValues: argv.cleanValues,
	emptyArrays: argv.emptyArrays,
	emptyObjects: argv.emptyObjects,
	emptyStrings: argv.emptyStrings,
	nullValues: argv.nullValues,
	undefinedValues: argv.undefinedValues
};

cleanJson(argv.input, argv.output, options, argv.spacing);

function cleanJson(input, output, options, spacing) {
	var inputString =  fs.readFileSync(
		input === '-' ? 0 : input, 
		'utf8'
	);
	var processedJson = cleanDeep.clean(inputString, options, spacing);
	fs.writeFileSync(
		output === '-' ? 1 : output,
		processedJson,
		'utf8'
	);
}
