# clean-deep-cli

This package provides a tiny CLI interface for [clean-deep](https://www.npmjs.com/package/clean-deep) library.

It allows a direct clean-up of `.json` files or standard input stream.

## Status

[![npm version][npm-image]][npm-url]

## Installation

Install only for current project:
```
$ npm install clean-deep-cli --save-dev
```

Install globally:
```
$ npm install -g clean-deep-cli
```
## Usage

Local installation:
```
$ npx clean-deep-cli [args]
```

Global installation:
```
$ clean-deep-cli [args]
```

Use `--help` as an argument to get a list of options.

## Examples

### Clean a local .json file

Content of `messy.json` file with empty values:
```
{
        "a": 1,
        "b": "",
        "c": {},
        "d": [],
        "e": [{"f": "", "g": null}]
}
```

Remove unused fields and write to `clean.json`:
```
$ npx clean-deep-cli -i messy.json -o clean.json
```

Content of `clean.json`:
```
{"a":1}
```

[npm-image]: https://img.shields.io/npm/v/clean-deep-cli.svg?style=flat
[npm-url]: https://www.npmjs.com/package/clean-deep-cli
